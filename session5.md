# What we covered in Session 5

## We worked on previous homework problems

## Additional Practice

1. Given 3 numbers num1, num2, and num3, use console.log to print out the largest of the 3 numbers. For example:

```javascript
   var num1 = 3;
   var num2 = 4;
   var num3 = 5;

   var largestNumber;

   // your code here

   console.log('The largest number is: ' + largestNumber);
   // Output: "The largest number is 5"
```

2. Given a letter, use console.log to print out whether the letter is a vowel. For example:

```javascript
   var letter = 'a';
   var isVowel;

   // your code here

   console.log('Is "' + letter + '" a vowel? ' + isVowel);
   // Output: "Is "a" a vowel? true"
```

3. Given a day of the week, use console.log to print out whether the day is a weekday or a weekend. For example:

```javascript
   var day = 'Monday';
   var dayType;

   // your code here

   console.log(day + ' is a ' + dayType);
   // Output: "Monday is a weekday"
```

4. Now we are going to check the day and the month to see what season it is. Spring will be from March 21 to June 20, Summer will be from June 21 to September 20, Autumn will be from September 21 to December 20, Winter will be from December 21 to March 20. Use console.log to print out what season it is. For example:

```javascript
   var month = 'April';
   var date = '27';
   var season;

   // your code here

   console.log('The season is ' + season);
   // Output: "The season is Spring"
```

5. Extend the above program to loop through every day of a 365 day year using for or while loops. For each day, print the month, the date, and the season using console.log.

```javascript
   // your code here

   // Output: "January 1, Winter"
   //         "January 2, Winter"
   //            .....
   //         "July 22, Summer"
   //            .....
   //         "December 31, Winter"
```

Every day should be printed out this way.
