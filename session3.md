# What we covered in Session 3

## Introduction to Functions

You’ve seen functions, such as `console.log`, and how to call them. Functions are the bread and butter of JavaScript programming. The concept of wrapping a piece of program in a function has many uses. It is a tool to structure larger programs, to reduce repetition, to associate names with subprograms, and to isolate these subprograms from each other.

## Defining a function

A function definition is just a regular variable definition where the value given to the variable happens to be a function. For example, the following code defines the variable `timesTwo` to refer to a function that produces two times a given number:

```javascript
var timesTwo = function(x) {
  return x * 2;
};

console.log(timesTwo(12)); // Output: 24

```

A function is created by an expression that starts with the keyword `function`. Functions have a set of **parameters** (in this case, only x) and a **body**, which contains the statements that are to be executed when the function is called. The function body must always be wrapped in braces `{` and `}`, even when it consists of only a single statement (as in the previous example).

A function can have multiple parameters or no parameters at all. In the following example, makeNoise does not list any parameter names, whereas power lists two:

```javascript
// Function definition
var makeNoise = function() {
  console.log(`Pling!`);
};
// Function invocation, ie, executing the function
makeNoise(); // Output: "Pling!"

var power = function(base, exponent) {
  var result = 1;
  for (var count = 0; count < exponent; count = count + 1)
    result = result * base;
  return result;
};

console.log(power(2, 10)); // Output: 1024
```

Some functions produce a value, such as `power` and `timesTwo`, and some don’t, such as `makeNoise`, which produces only a side effect. A `return` statement determines the value the function returns. When control comes across such a statement, it immediately jumps out of the current function and gives the returned value to the code that called the function. The `return` keyword without an expression after it will cause the function to return `undefined`.

## Parameters and Scopes

The **parameters** to a function behave like regular variables, but their initial values are given by the *caller* of the function, not the code in the function itself.

An important property of functions is that the variables created inside of them, including their parameters, are **local** to the function. This means, for example, that the `result` variable in the `power` example will be newly created every time the function is called, and these separate incarnations do not interfere with each other.

This “local-ness” of variables applies only to the parameters and to variables declared with the var keyword inside the function body. Variables declared outside of any function are called **global**, because they are visible throughout the program. It is possible to access such variables from inside a function, as long as you haven’t declared a local variable with the same name.

The following code demonstrates this. It defines and calls two functions that both assign a value to the variable `x`. The first one declares the variable as **local** and thus changes only the local variable. The second does not declare `x` locally, so references to `x` inside of it refer to the **global** variable `x` defined at the top of the example.

```javascript
var x = "outside";

var f1 = function() {
  var x = "inside f1";
};
f1();
console.log(x); // Output: "outside"

var f2 = function() {
  x = "inside f2";
};
f2();
console.log(x); // Output: "inside f2"
```

This behavior helps prevent accidental interference between functions. If all variables were shared by the whole program, it’d take a lot of effort to make sure no name is ever used for two different purposes. And if you *did* reuse a variable name, you might see strange effects from unrelated code messing with the value of your variable. By treating function-local variables as existing only within the function, the language makes it possible to read and understand functions as small universes, without having to worry about all the code at once.

## Homework

Rather than have just one homework problem, I am going to assign a series of short problems.

1. Rewrite FizzBuzz from [Session 1](https://gitlab.com/aezed/us-coding-club/blob/master/session1.md) as a function named `fizzBuzz`. The new function should take one parameter `n` which corresponds to the number `fizzBuzz` will count to, rather than just counting to 100.

1. Write a function `maximum` that takes 2 parameters, `num1` and `num2`. Return the greater of the 2 numbers. If the numbers are equal, return the string `equal`. Test whether your function works using `console.log`.

OPTIONAL EXTRA CREDIT: You can get a string's length with, for example, `"s".length`. This will give you the number of characters in the string. Similarly, you can get the Nth character from a string by writing `"string".charAt(N)`. The returned value will be a string containing only one character (for example, "b"). The first character has position zero, which causes the last one to be found at position `string.length - 1`. In other words, a two-character string has length 2, and its characters have positions 0 and 1.

Write a function `countBs` that takes a string as its only argument and returns a number that indicates how many uppercase “B” characters are in the string.

Next, write a function called `countChar` that behaves like countBs, except it takes a second argument that indicates the character that is to be counted (rather than counting only uppercase “B” characters). Rewrite `countBs` to make use of this new function.

```javascript
// Your code here.

console.log(countBs("BBC")); // Output: 2

console.log(countChar("kakkerlak", "k")); // Output: 4

```
