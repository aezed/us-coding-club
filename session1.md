# What we covered in Session 1

## Overview

We will be learning Javascript together in this course. Javascript is a very practical first computer programming language to learn, as it is embedded in every web browser. It powers the front-end of all modern web applications, and increasingly the back-end as well.

## Running your Code

First, go to [JSBin](https://jsbin.com). In the upper-right of the *Output* pane, un-check *Auto-run JS*. Then go ahead and de-select the **HTML**, **CSS** and **Output** panes. Copy and paste the code sections into the JavaScript pane and play with the values and outputs in the console. Make sure you understand what's going on, and please ask questions in #us-code-club on Slack. No question is too basic.

## Print to the javascript console

```javascript
// Anything after "//" on a line is a "comment" and will not
// be executed. Otherwise, Javascript executes each line sequentially.

console.log('Hello, world!'); // Output: "Hello, world!"
```

## Create and use variables

```javascript
// use the var statement to create and initialize variables
// values contained in 'single-quotes' are called strings

var myFirstVariable = 'Hello';
var mySecondVariable = ', world!';

// You can use the + operator to concatenate two strings
var myThirdVariable = myFirstVariable + mySecondVariable;

console.log(myThirdVariable); // Output: "Hello, world!"

myFirstVariable = 3;
mySecondVariable = 2;

// You can also use arithmetic operators to do math like a calculator.
// Arithmetic operators are +, -, *, /, and % (modulus or remainder)
myThirdVariable = myFirstVariable + mySecondVariable;

console.log(myThirdVariable); // Output: 5
console.log(myThirdVariable * 2) // Output: 10
```

## Create and use `for` and `while` loops

```javascript
for (var i = 1; i < 4; i = i + 1) {
    console.log('Hello #' + i + '!');
}

// Output:
// "Hello #1!"
// "Hello #2!"
// "Hello #3!"

// The above "for" loop is equivalent to the below "while" loop:

var i = 1;
while (i < 4) {
    console.log('Hello #' + i + '!');
    i = i + 1;
}

// Output:
// "Hello #1!"
// "Hello #2!"
// "Hello #3!"
```

## Working with conditionals: `if` / `else if` / `else` statements

```javascript
// A boolean expression is an expression that evaluates to either true or false,
// generally using comparison and logical operators. Comparison operators are
// > (greater than), < (less than), >= (greater than or equal to),
// <= (less than or equal to), === (equal to), !== (not equal to).
// Logical operators are && (AND), || (OR), ! (NOT)

var booleanValue = true;

if (booleanValue === true) {
    console.log('The value is true');
} else {
    console.log('The value is false');
}

// Output: "The value is true"

booleanValue = false;

if (booleanValue === true) {
    console.log('The value is true');
} else {
    console.log('The value is false');
}

// Output: "The value is false"

var thisVariable = 3;

if (thisVariable === 3) {
    console.log('Yup, three.');
} else {
    console.log('Not three.');
}

// Output: "Yup, three."

if (thisVariable < 3) {
    console.log('Less than three.');
} else if (thisVariable >= 3) {
    console.log('Greater than or equal to three.');
} else {
    console.log('Some other thing');
}

// Output: "Greater than or equal to three."

if (thisVariable <= 2 && booleanValue !== true) {
    console.log('Less than or equal to two AND false');
} else if (thisVariable <= 2 || booleanValue !== true) {
    console.log('Less than or equal to two OR false');
} else if (!(thisVariable <= 2 || booleanValue !== true)) {
    console.log('NOT (Less than or equal to OR false)');
} else {
    console.log('Neither of the above conditions are true');
}

// Output: "Less than or equal to two OR false"
```

## Homework

Write a program that prints the numbers from 0 to 100. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”.

Don't look for solutions online, but please feel free to use [MDN Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) as a reference.
