# U.S. Coding Club

## Session Reviews

[Session 1](https://gitlab.com/aezed/us-coding-club/blob/master/session1.md) Variables, loops, and conditionals

[Session 2](https://gitlab.com/aezed/us-coding-club/blob/master/session2.md) Session 1 review

[Session 3](https://gitlab.com/aezed/us-coding-club/blob/master/session3.md) Introduction to functions

[Session 4](https://gitlab.com/aezed/us-coding-club/blob/master/session4.md) Session 3 review

[Session 5](https://gitlab.com/aezed/us-coding-club/blob/master/session5.md) All Sessions review
## Resources

[MDN Javascript Reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript): This is the best Javascript reference. Use it liberally.
