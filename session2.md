# What we covered in Session 2

## We reviewed Session 1

[Session 1 notes + Homework](https://gitlab.com/aezed/us-coding-club/blob/master/session1.md)

## Homework

Initialize 3 variables, ‘card1’, ‘card2’, and 'card3'. Assign each variable a value between 1 and 11.

You're writing a program to return a BlackJack hand. Print the sum of card1, card2, and card3, but if the sum is greater than 21, print "bust", unless one of the numbers is 11. In such a case, the 11 should be 'converted' to a 1 to prevent the sum from being exceeded.
For example, given a 11, 10, and 3 as input, the 11 should be 'converted' into a 1 so the total sum will be 14.
