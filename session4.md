# What we covered in Session 4

## We reviewed Session 3

[Session 3 notes + Homework](https://gitlab.com/aezed/us-coding-club/blob/master/session3.md)

## Homework

If we want to convert a string to all lowercase letters, we have access to the method `.toLowerCase`. Similarly, we convert strings to all uppercase letters using `.toUpperCase`. For example:

```javascript
var fish = 'I want a fish. I LOVE fish.';
var lowerFish = fish.toLowerCase();
console.log(lowerFish); // Output: 'i want a fish. i love fish.'
var upperFish = fish.toUpperCase();
console.log(upperFish); // Output: 'I WANT A FISH. I LOVE FISH.'
```

Write a function `altercase` that takes a string and converts it such that the letters alternate cases, starting with uppercase. For example:

```javascript
var fish = 'I want a fish. I love fish.';
var alterFish = altercase(fish);
console.log(alterFish); // Output: 'I wAnT a FiSh. I lOvE fIsH.'
```

Recall that you can get the length of a string from the `length` property, as in `'fish'.length // 4`. You may also find the method `.charAt` useful. It returns the character at the position in the string, starting from `0` and ending at `string.length - 1`. For example, `fish.charAt(1) // "f"`. You can find more info about `.charAt` and other string methods on [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript).
